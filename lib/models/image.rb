class Image < ActiveRecord::Base
	belongs_to :imageable, polymorphic: true

   attr_accessible :photo
   has_attached_file :photo, :styles => { :small_blog => "250x250#", :large_blog => "680x224#", :thumb => "95x95#", :portfolio_square => "300x300#", :portfolio_small => "220x220#", :portfolio_large => "680x680#" },
    :storage => :s3,
    :url  => ":s3_domain_url",
    :s3_protocol => 'http',
    :path => "/images/:id/:style.:extension",
    :s3_credentials => {
     :bucket => ENV['AWS_BUCKET'],
     :access_key_id => ENV['AWS_ACCESS_KEY_ID'],
     :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY']
    }
end

  

