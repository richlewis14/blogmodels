class User < ActiveRecord::Base

  attr_accessible :email, :password, :password_confirmation, :remember_me, :admin, :name
  
  has_many :posts
end
