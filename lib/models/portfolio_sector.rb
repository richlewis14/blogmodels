class PortfolioSector < ActiveRecord::Base
	#Holds portfolio_id and sector_id to allow multiple sectors to be saved per portfolio piece, as opposed to storing an array of objects in one DB column
	belongs_to :portfolio
    belongs_to :sector
end

