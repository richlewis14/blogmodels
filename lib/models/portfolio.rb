class Portfolio < ActiveRecord::Base
    extend FriendlyId
    friendly_id :title, use: :slugged
    
    has_many :portfolio_sectors
    has_many :sectors, through: :portfolio_sectors
    has_many :images, as: :imageable, :dependent => :destroy

    accepts_nested_attributes_for :images
    attr_accessible :overview, :title, :url, :sector_ids, :image_id, :images_attributes
    
    #Validations
    validates :title, :presence => {:message => 'Add your Title'}
    validates :url, :presence => {:message => 'Add a URL'}
    validates :overview, :presence => {:message => 'Add an Overview'}
    validates :sector_ids, :presence => {:message => 'Choose At Least 1 Sector'}
    
    

 def previous_post
  self.class.first(:conditions => ["title < ?", title], :order => "title desc")
 end

 def next_post
  self.class.first(:conditions => ["title > ?", title], :order => "title asc")
 end
  
end

