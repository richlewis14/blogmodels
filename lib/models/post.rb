class Post < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged

  belongs_to :category
  belongs_to :user
  has_many :images, as: :imageable, :dependent => :destroy

  accepts_nested_attributes_for :images
  attr_accessible :comments, :title, :category_id, :user_id, :image_id, :images_attributes, :imageable_id, :imageable_attributes, :slug

  
  #Validations
  validates :comments, :presence => {:message => 'Add your Comments'}
  validates :title, :presence => {:message => 'Add your Title'}

  #scopes
  scope :latest_posts, :order => "posts.created_at DESC"
  #scope :ruby_posts, :include => :category, :conditions => {"categories.name" => "Ruby"}, :order => "posts.created_at DESC"
  

  def self.search(search)
    where("title like ?", "%#{search}%")
  end

end
