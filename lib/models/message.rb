class Message
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_accessor :name, :email, :website, :message, :ghost

  validates :name, :presence => {:message => 'Add your Name'}
  validates :email, :presence => {:message => 'Add your Email Address'}  
  validates :message, :presence => {:message => 'Add your Message'} 
  validate :check_for_spam

  def check_for_spam
    errors.add :ghost if ghost.present?
  end
  
  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end

 

end

