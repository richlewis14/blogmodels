class Sector < ActiveRecord::Base
	has_many :portfolio_sectors
    has_many :portfolios, through: :portfolio_sectors
    
    attr_accessible :name
end

