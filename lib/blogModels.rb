require "blogModels/version"

module BlogModels

Gem.find_files("models/*.rb").each do |f| 
  filename = File.basename(f, '.*')
  class_name_symbol = filename.classify.to_sym
  autoload class_name_symbol, "models/#{filename}"
end

end
